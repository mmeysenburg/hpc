# Mark Meysenburg's HPC repo

This repository contains code for experimenting with HTC/HPC. 

Mark M. Meysenburg

<mark.meysenburg@doane.edu>

## Sample code

- cpp-mpi: Simple OpenMPI example, getting a response from the specified number
of threads. 

- hostname: Simple hostname example that gets the hostname for several nodes 
on the cluster, and appends the names to the end of a text file.

- htc-pi: Monte Carlo pi estimation, HTC approach; each compute node runs a 
separate process, and outputs the number of "darts" landing in a unit circle.
Then, a separate process totals the number of darts in the circle over all 
processes, multiplies by 4, and divides by the number of darts thrown,
resulting in an estimate for pi.
