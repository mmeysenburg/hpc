#include "float.h"
#include "limits.h"
#include "math.h"
#include "mpi.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"

// function prototypes for GA operators
void crossover(double **ppdPopulation, size_t popSize, size_t chromSize,
               double chi);
double fitness(double *pChromosome, size_t chromSize);
void mutate(double **ppdPopulation, size_t popSize, size_t chromSize, double lo,
            double hi, double mu);
void selection(double **ppdPopulation, double **ppdTempPop, double *pFitnesses,
               size_t popSize, size_t chromSize, size_t k);

// function prototypes for utilities
double nextDouble();
double nextDoubleRange(double lo, double hi);
int nextInt(int lo, int hi);

/**
 * Simple MPI-enabled genetic algorithm, using double chromosomes, point 
 * mutation and point crossover, with non-elitist tournament selection. 
 * 
 * Right now this program runs a completely separate population on each 
 * processor; each slave process then reports its best-ever fitness and 
 * individual back to the master process, which reports the results. 
 *
 * \author Mark M. Meysenburg
 * \version 12/1/2018
 *
 */
int main(int iArgc, char **ppszArgv) {
  // generic variables
  size_t i, j;
  double f;

  // MPI variables
  int mpiRank;     // rank number of this process
  int mpiNumProcs; // number of processes

  // GA-specific variables
  double bestEverFitness; // fitness of best-ever individual
  double chi;             // probability of a crossover
  size_t chromLen;        // number of genes per chromosome
  size_t gen;             // current generation number
  size_t k;               // tournament size
  double low, high;       // legal range of a gene: [low, high]
  double mu;              // probability of a mutation
  size_t numGens;         // number of generations to run
  double *pBestEver;      // best-ever individual
  double *pFitnesses;     // array holding fitness of each individual
  size_t popSize;         // size of the population
  double **ppdPopulation; // array of arrays of doubles holding the population
  double **ppdTemp;       // array of arrays of doubles holding temporary population

  // initialize run parameters
  popSize = 25000;
  numGens = 1000;
  low = -600.0;
  high = 600.0;
  chromLen = 10;
  chi = 0.6;
  mu = 0.01;
  k = 2;
  bestEverFitness = -DBL_MAX;

  // initialize MPI system
  MPI_Init(&iArgc, &ppszArgv);

  // which process are we?
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);

  // how many processes are there?
  MPI_Comm_size(MPI_COMM_WORLD, &mpiNumProcs);

  // now fork based on which process we are
  if (mpiRank == 0) {
    // root process? collect results from sub-processes

    // first allocate space for chromosome
    pBestEver = (double *)malloc(sizeof(double) * chromLen);

    // now receive results from slaves
    for (i = 1; i < mpiNumProcs; i++) {

      // best fitness from process i
      MPI_Recv(&bestEverFitness, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);

      // best individual from process i
      MPI_Recv(pBestEver, chromLen, MPI_DOUBLE, i, 1, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);

      // output results
      printf("Process: %lu\tfitness: %0.6f\n", i, bestEverFitness);
      printf("\t[");
      for (j = 0; j < chromLen - 1; j++) {
        printf("%0.6f, ", pBestEver[j]);
      }
      printf("%0.6f]\n", pBestEver[chromLen - 1]);
    }

    // free memory for chromosome
    free(pBestEver);

  } else {
    // slave process? do the GA and send results to root
    printf("Process %d starting GA...\n", mpiRank);

    // seed PRNG differently for each slave processes
    srand(time(NULL) + mpiRank);

    // allocate space for fitnesses and for primary and temporary populations
    pFitnesses = (double *)malloc(sizeof(double) * popSize);
    ppdPopulation = (double **)malloc(sizeof(double *) * popSize);
    ppdTemp = (double **)malloc(sizeof(double *) * popSize);
    pBestEver = (double *)malloc(sizeof(double) * chromLen);

    // allocate each chromosome, and fill population chromosomes with random
    // values
    for (i = 0; i < popSize; i++) {
      ppdPopulation[i] = (double *)malloc(sizeof(double) * chromLen);
      ppdTemp[i] = (double *)malloc(sizeof(double) * chromLen);
      for (j = 0; j < chromLen; j++) {
        ppdPopulation[i][j] = nextDoubleRange(low, high);
      }
    }

    // do the GA!
    for (gen = 0; gen < numGens; gen++) {
      // crossover
      crossover(ppdPopulation, popSize, chromLen, chi);

      // mutation
      mutate(ppdPopulation, popSize, chromLen, low, high, mu);

      // evaluate
      for (i = 0; i < popSize; i++) {
        pFitnesses[i] = fitness(ppdPopulation[i], chromLen);
      }

      // selection
      selection(ppdPopulation, ppdTemp, pFitnesses, popSize, chromLen, k);

      // keep track of best-ever individual and its fitness
      for (i = 0; i < popSize; i++) {
        f = pFitnesses[i];
        if (f > bestEverFitness) {
          bestEverFitness = f;
          for (j = 0; j < chromLen; j++) {
            pBestEver[j] = ppdPopulation[i][j];
          }
        }
      }
    }

    // report best ever individual & results
    MPI_Send(&bestEverFitness, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    MPI_Send(pBestEver, chromLen, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);

    // free space for the fitnesses and best ever
    free(pFitnesses);
    free(pBestEver);

    // free space for populations
    for (i = 0; i < popSize; i++) {
      free(ppdPopulation[i]);
      free(ppdTemp[i]);
    }
    free(ppdPopulation);
    free(ppdTemp);
  }

  // processed join here; finalize MPI and exit
  MPI_Finalize();

  return EXIT_SUCCESS;
}

/**
 * Function to perform single-point crossover on a population of double
 * chromosomes. 
 *
 * \param ppdPopulation 2d array of doubles representing the population
 *
 * \param popSize Number of individuals in the population
 *
 * \param chromSize Length of each individual in the population
 *
 * \param chi Probability that an individual undergoes crossover
 */
void crossover(double **ppdPopulation, size_t popSize, size_t chromSize,
               double chi) {

  size_t i, dad, mom, idx;
  double d;

  // give each individual a chance to crossover...
  for (dad = 0; dad < popSize; dad++) {

    // do crossover?
    if (nextDouble() <= chi) {
  
      // if so, pick a mate...
      mom = nextInt(0, popSize);

      // ... pick a crossover point...
      idx = nextInt(0, chromSize);

      // ... and swap between mom and dad after that point
      for (i = idx; i < chromSize; i++) {

        d = ppdPopulation[dad][i];
        ppdPopulation[dad][i] = ppdPopulation[mom][i];
        ppdPopulation[mom][i] = d;
      } // for genes

    } // if crossover

  } // for each individual 

} // crossover function

/** 
 * Evaluate the fitness of an individual composed of doubles. Evaluate as a 
 * solution to Griewank's Function; see 
 * http://mathworld.wolfram.com/GriewankFunction.html
 *
 * \param pChromosome Array of doubles representing this individual
 *
 * \param chromSize Length of the individual
 *
 * \return Fitness of the individual
 */
double fitness(double *pChromosome, size_t chromSize) {
  size_t i;
  double product = 1.0;
  double sum = 0.0;

  for (i = 0; i < chromSize; i++) {
    sum += (pChromosome[i] * pChromosome[i]) / 4000.0;
    product *= cos(pChromosome[i]) / sqrt(i + 1.0);
  }

  return -1.0 - sum + product;

} // fitness

/** 
 * Function to perform single point mutation on a population of double 
 * chromosomes.
 * 
 * \param ppdPopulation 2d array of doubles representing the population
 *
 * \param popSize Number of individuals in the population
 *
 * \param chromSize Lenght of an individual in the population
 *
 * \param lo Lowest possible value for a mutated gene
 *
 * \param hi Highest possible value for a mutated gene
 *
 * \param mu Probability of an individual undergoing mutation
 */
void mutate(double **ppdPopulation, size_t popSize, size_t chromSize, double lo,
            double hi, double mu) {

  size_t i, idx;

  // look at each individual in the population
  for (i = 0; i < popSize; i++) {

    // do mutation?
    if (nextDouble() <= mu) {

      // if so, pick a point...
      idx = nextInt(0, chromSize);

      // ... and change that value to a new one
      ppdPopulation[i][idx] = nextDoubleRange(lo, hi);

    } // do a mutation

  } // for each individual

} // mutate

/**
 * Perform non-elitist tournament selection on a population of double 
 * chromosomes. 
 *
 * \param ppdPopulation 2d array of doubles representing the population
 *
 * \param ppdTempPop 2d array of same size as population, used during selection
 * 
 * \param pFitnesses 1d array of fitnesses, corresponding to the members of the
 * population
 *
 * \param popSize Number of individuals in the population
 *
 * \param chromSize Length of an individual in the population
 *
 * \param k Number of individuals that take part in each tournament
 */
void selection(double **ppdPopulation, double **ppdTempPop, double *pFitnesses,
               size_t popSize, size_t chromSize, size_t k) {

  size_t i, j, idx, maxIdx;
  double maxF, f;

  // fill temp pop
  for (i = 0; i < popSize; i++) {

    maxF = -DBL_MAX;
    maxIdx = INT_MAX;

    // do a k tournament
    for (j = 0; j < k; j++) {

      idx = nextInt(0, popSize);
      f = pFitnesses[idx];
      if (f > maxF) {
        maxF = f;
        maxIdx = idx;
      }
    }

    // save the winner
    for (j = 0; j < chromSize; j++) {
      ppdTempPop[i][j] = ppdPopulation[maxIdx][j];
    }
    pFitnesses[i] = maxF;

  } // temp pop filler

  // copy temp to main population
  for (i = 0; i < popSize; i++) {

    for (j = 0; j < chromSize; j++) {

      ppdPopulation[i][j] = ppdTempPop[i][j];

    } // for gene

  } // for individual 

} // selection

/**
 * Get a pseudo-random double in the range [0, 1].
 */
double nextDouble() { return (double)rand() / ((double)(RAND_MAX)); }

/** 
 * Get a pseudo-random double in the range [lo, hi].
 */
double nextDoubleRange(double lo, double hi) {
  double r;
  r = (double)rand() / ((double)(RAND_MAX));
  r = lo + (r * (hi - lo));
  return r;
}

/**
 * Get a pseudo-random int in the range [lo, hi].
 */
int nextInt(int lo, int hi) { return lo + (int)(nextDouble() * (hi - lo - 1)); }
