# aggregate.py: sum the numbers from the htc-pi output, then multiply by 4
# and divide by the number of total darts thrown; this gives the estimate
# of the value of pi.
#
# usage: cat <filename> | python aggregate.py
#
# Mark M. Meysenburg
# 11/20/2018
import sys

# initialize accumulator
acc = 0.0
numLines = 0

# read input from standard input
for line in sys.stdin:
	acc += float(line)
	numLines += 1

# do the math
pi = (4.0 * acc) / (2000000000.0 * numLines)

# output the results!
print(pi)
