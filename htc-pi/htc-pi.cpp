
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <omp.h>
#include <random>

using namespace std;

// number of random darts thrown by the program
#define NUM_DARTS 2000000000

/**
 * Monte Carlo estimation of pi, using a HTC approach. This program throws
 * NUM_DARTS pseudo-random darts at a unit square, and counts the number 
 * of darts that land inside a unit circle contained in that square. This number,
 * multiplied by 4 and divided by NUM_DARTS, produces an estimate of the value
 * of pi. This program simply outputs the number of darts landing in the circle.
 * Another process sums up the values produced by each program running on
 * the individual nodes, multiplies by 4, and divides by the total number of darts
 * thrown. 
 *
 * \author Mark M. Meysenburg
 * \version 11/20/2018
 * 
 */
int main() {

	// number of darts landing in the unit circle
    long numIn = 0L;

// start of OMP parallel portion of the program; the following is run on each 
// core, with the for loop spread out over the cores.
#pragma omp parallel reduction(+:numIn)
{
	// PRNG must be allocated one per core, because the mt19937 generator
	// is not thread-safe. Here each generator is allocated using the system time
    mt19937_64 prng(chrono::system_clock::now().time_since_epoch().count());
	// distribution to map sequence produced by mt19937 PRNG to the real 
	// range [-1, 1).
    uniform_real_distribution<long double> dis(-1.0, 1.0);

	// coordinates for location of each dart thrown
    long double x, y;

// OMP parallelizes this for loop over the available cores automatically
#pragma omp for
	// throw the darts
    for(int i = 0; i < NUM_DARTS; i++) {
        x = dis(prng);
        y = dis(prng);

		// is this dart in the unit circle? This uses the Euclidean distance 
		// between the dart and the origin, without bothering to calculate
		// the square root
        if( x * x + y * y <= 1.0) {
            numIn++;
        } // if
    } // for 
} // parallel section

	// output the number of darts than landed in the circle
    cout << numIn << endl;

    return EXIT_SUCCESS;
}